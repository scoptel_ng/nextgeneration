######################################################################################
#                           ---==== Code Header ===---
#
#   Copyright 2004-2018 ScopServ International Inc. <support@scopserv.com>
#
#
#   Method [Description]:
#
#
#   Purpose:  Explain what this method does to support the correct
#             operation of its class, and how it does it.
#
#
#   Pre-Condition:  Any non-obvious conditions that must exist
#                   or be true before we can expect this method to function
#                   correctly.
#
#   Post-condition: What we can expect to exist or be true after
#                   this method has executed under the pre-condition(s).
#
#
#   Parameters: Explanation of the purpose of this
#               parameter to the method.  Write one explanation for each
#               formal parameter of this method.
#
#   Returns: If this method sends back a value via the return
#            mechanism, describe the purpose of that value here, otherwise state 'None.'
#
#                           ---==== Code Header ===---
######################################################################################


# you code here


######################################################################################
#                           ---=== Code Footer ===---
#
#   Version:
#
#   Correction:
#
#   To Do:
#
#   Need to modify:
#                           ---=== Code Footer ===---
######################################################################################

