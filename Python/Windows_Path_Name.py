# ---------------------------------------------------------------------------------------//
# ------- Change Dash Direction on Windows OS and convert to Multiplatform Path ---------//
# ------- Author : Maxime Ferron / mferron@scopserv.com / 2017-10-25 --------------------//
# ---------------------------------------------------------------------------------------//

# My_Path = 'C:\Users\Desktop\new_text.txt'

# This line cause error because python see \n for change line.
# This is not problem in Linux, Unix or OSX. The path slash in Windows OS is in other direction and cause problem.
# If you put r before your path, correct this problem, but no resolve multi-platform slash path direction.
# This is the trick, the resolve copy/paste of path in variable in windows OS and convert for all system.

# ------  Example with print out -----------
My_Path = r'C:\Users\Desktop\new_text.txt'
print(My_Path)
My_Path = My_Path.replace('\\', '/')
print(My_Path)
# ------  Example with print out -----------


# --------------- Solution ---------------
My_Path = r'C:\Users\Desktop\new_text.txt'
My_Path = My_Path.replace('\\', '/')
# --------------- Solution ---------------

# ---------------------------------------------------------------------------------------//
# ------- Change Dash Direction on Windows OS and convert to Multiplatform Path ---------//
# ------- Tested By :                                                           ---------//
# ---------------------------------------------------------------------------------------//

