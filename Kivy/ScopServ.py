# -*- coding: utf-8 -*-
__title__ = 'ScopServNG'
__author__ = '@ScopServ / Maxime Ferron / 2017-18'

BuiltVersion = "0.2"

############################################################
# ----------------- Module Import ------------------------ #
############################################################
import os
import sys
import platform
import struct

import kivy
from kivy.app import App
from kivy.lang import Builder
from kivy.base import runTouchApp

from kivy.core.window import Window
from kivy.factory import Factory

from kivy.uix.floatlayout       import FloatLayout
from kivy.uix.boxlayout         import BoxLayout
from kivy.uix.stacklayout       import StackLayout
from kivy.uix.anchorlayout      import AnchorLayout
from kivy.uix.gridlayout        import GridLayout
from kivy.uix.pagelayout        import PageLayout
from kivy.uix.relativelayout    import RelativeLayout
from kivy.uix.scatterlayout     import ScatterLayout

# from kivy.uix.actionbar import ActionBar
# from kivy.uix.actionbar import ActionView
# from kivy.uix.actionbar import ActionButton
# from kivy.uix.actionbar import ActionPrevious
# from kivy.uix.actionbar import ActionOverflow
# from kivy.uix.actionbar import ActionItem
# from kivy.uix.actionbar import ActionBarException

from kivy.uix.scatter import Scatter
from kivy.graphics.svg import Svg
from kivy.animation import Animation
from kivy.uix.stencilview import StencilView
from kivy.uix.screenmanager import Screen, ScreenManager, FadeTransition
from kivy.properties import BooleanProperty, StringProperty, DictProperty, ObjectProperty, ListProperty, NumericProperty
from kivy.uix.togglebutton import ToggleButton
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from kivy.uix.behaviors import FocusBehavior
from kivy.uix.popup import Popup
from kivy.clock import Clock, mainthread
from kivy.uix.dropdown import DropDown

from kivy.uix.image import Image
from kivy.uix.widget import Widget


kivy.require('1.10.0')
############################################################
# ----------------- Module Import ------------------------ #
############################################################
# --
# --
############################################################
#  ----------------- Kivy Path Control ---------------------
############################################################
os.environ['KIVY_TEXT'] = 'pil'
os.environ['KIVY_DATA_DIR'] = ''
os.environ['KIVY_MODULES_DIR'] = ''
os.environ['KIVY_HOME'] = ''
os.environ['KIVY_SDL2_PATH'] = ''
############################################################
#  ----------------- Kivy Path Control ---------------------
############################################################
# --
# --
############################################################
# ----------------- Kivy Configuration ---------------------
############################################################
from kivy.config import Config

Config.set("kivy", "exit_on_escape", False)
Config.set("graphics", "height", 660)
Config.set("graphics", "width", 1340)
Config.set('graphics', 'show_cursor', 1)
window_icon = ''
if Config:  window_icon = Config.get('kivy', 'window_icon')
os.environ["KIVY_USE_DEFAULTCONFIG"] = "0"  # -- If this name is found in environ, Kivy will not read the user config#  file.
os.environ["KIVY_NO_CONFIG"] = "0"  # -- If set, no configuration file will be read or written to. This also applies to the user configuration directory.
os.environ["KIVY_NO_FILELOG"] = "0"  # -- KIVY_NO_FILELOG
os.environ["KIVY_NO_CONSOLELOG"] = "0"  # -- If set, logs will be not print to the console
os.environ["KIVY_NO_ARGS"] = "0"  # -- If set, the argument passed in command line will not be parsed and used by Kivy. Ie, you can safely make a script or an app with your own arguments without requiring the – delimiter:
############################################################
# ----------------- Kivy Configuration ---------------------
############################################################
# --
# --


# with open('scopserv.kv', encoding='utf8') as f:
#     # u = unicode(f, "utf-8")
#     f = f.encode("utf-8")
#     Builder.load_string(f.read())

# LoadKivyFile = Builder.load_string('scopserv.kv') # give error??

# runTouchApp(Builder.load_string ('''
#
# <MainWindow>:
#     ActionBar:
#         size_hint: 1,0.1
#         pos_hint: {'top':1}
#         ActionView:
#             ActionPrevious:
#                 title: 'Title'
#             ActionOverflow:
#             ActionButton:
#                 text: 'Btn0'
#             ActionButton:
#                 text: 'Btn1'
#             ActionButton:
#                 text: 'Btn2'
#             ActionButton:
#                 text: 'Btn3'
#             ActionButton:
#                 text: 'Btn4'
#             ActionGroup:
#                 text: 'Group1'
#                 mode: 'spinner'
#                 ActionButton:
#                     text: 'Btn5'
#                 ActionButton:
#                     text: 'Btn6'
#                 ActionButton:
#                     text: 'Btn7'
# '''))

class MainWindow(FloatLayout):
    pass

def keypress(event):
    print('key down', event.key)

class ActionBarMain(Screen):

    fullscreen = BooleanProperty(True)

    def __init__(self):
        super(ActionBarMain, self).__init__()
    #
    # def add_widget(self, *args):
    #     if 'content' in self.ids:
    #         return self.ids.content.add_widget(*args)
    #     return super(ActionBarMain, self).add_widget(*args)

    # def on_dismiss_screenmanager(self):
    #     app.show_main_screen()


class SystemInfoDesktop(AnchorLayout):

    SystemOS = StringProperty('')
    PythonVersionOS = StringProperty('')
    BuiltVersionOS = StringProperty('')

    def __init__(self, **kwargs):
        super(SystemInfoDesktop, self).__init__(**kwargs)
        self.SystemOS = MyOs
        self.PythonVersionOS = PythonVersion
        self.BuiltVersionOS = BuiltVersion

# ----------------------------------------------------


class screen1(Screen): pass
class screen2(Screen): pass
class screen3(Screen): pass


class MyScreenManager(ScreenManager):


    fullscreen = BooleanProperty(True)
    background_image = ObjectProperty(Image(source='assets/img/bg/background.jpg'))
    screen1 = ObjectProperty(None)
    screen2 = ObjectProperty(None)
    screen3 = ObjectProperty(None)


    def __init__(self):
        super(MyScreenManager, self).__init__()

    #
    # def add_widget(self, *args):
    #     if 'content' in self.ids:
    #         return self.ids.content.add_widget(*args)
    #     return super(MyScreenManager, self).add_widget(*args)


    # def on_dismiss_screenmanager(self):
    #     app.show_main_screen()
# ----------------------------------------------------
# --

# --------------------------------------------------
class AnchorLayout(AnchorLayout): pass
# --------------------------------------------------
# --
# -------------------------------------
class StackLayout(StackLayout): pass
# -------------------------------------
# --
# -------------------------------------
class GridLayout(GridLayout):  pass
# -------------------------------------
# --
# -------------------------------------
class BoxLayout(BoxLayout):  pass
# -------------------------------------
# --
# -------------------------------------
class FloatLayout(FloatLayout): pass
# -------------------------------------
# --
# -------------------------------------
class PageLayout(PageLayout):  pass
# -------------------------------------
# --
# -------------------------------------
class RelativeLayout(RelativeLayout):  pass
# -------------------------------------
# --
# -------------------------------------
class ScatterLayout(ScatterLayout):
    pass
# -------------------------------------



# class MyActionBar(FloatLayout):
#     def __init__(self,**kwargs):
#         super(MyActionBar,self).__init__(**kwargs)
#         self.add_widget(MyActionBar())

# -------------------------------------------



##--
##--
############################################################
# ------------------- Python Version --------------------- #
############################################################
PythonVersion = (sys.version)       #-- System Report Python Version
PythonVersion = PythonVersion[:5]   #-- Select 5 first letter in String
#print (PythonVersion)
############################################################
# ------------------- Python Version --------------------- #
############################################################
##--
##--
############################################################
# ------------------- Platform OS ------------------------ #
############################################################
import winreg

try:

    if sys.platform == "linux" or sys.platform == "linux2":
        MyOs = "linux"
    elif sys.platform == "darwin": #-- Need to convert to detect osx architecture
        MyOs = "MacOsX"
    elif sys.platform == "win32":

        def get_registry_value(key, subkey, value):
            key = getattr(winreg, key)
            handle = winreg.OpenKey(key, subkey)
            (value, type) = winreg.QueryValueEx(handle, value)
            return value

        windowsbit = cputype = get_registry_value("HKEY_LOCAL_MACHINE","SYSTEM\\CurrentControlSet\Control\\Session Manager\\Environment","PROCESSOR_ARCHITECTURE")

        if windowsbit == "AMD64":
            MyOs = "Windows 64 Bits"
        else:
            MyOs = "Windows 32 Bits"

except (RuntimeError, TypeError, NameError):
    print("Error")

############################################################
# ------------------- Platform OS ------------------------ #
############################################################



class ScopServApp(App):

    def build(self):
        Window.size = (1340, 660)
        # Window.clearcolor = (0, 0, 0, 1)
        # Window.borderless = True
        self.icon = 'assets/icons/ico/bar.ico'
        return self.root
        #return AnchorLayout()
        #return StackLayout()
        #return LoadKivyFile()
        #return GridLayout()
        #return BoxLayout()
        #return FloatLayout()
        #return PageLayout()
        #return RelativeLayout()
        #return ScatterLayout()
        #return SystemInfoDesktop()
        #return ActionBarMain()
        #return MainWindow()

        # layout = GridLayout(cols=1, spacing=10, size_hint_y=None)
        # layout.bind(minimum_height=layout.setter('height'))
        # actionbar = ActionBar(pos_hint={'top': 1.0})
        # actionview = ActionView()
        # actionbar.add_widget(actionview)
        # # actionbutton = ActionButton(text='Fram')
        # # actionview.add_widget(actionbutton)
        # layout.add_widget(actionbar)
        #
        # for i in range(30):
        #     btn = Label(text=str(i), size_hint_y=None, height=40)
        #     layout.add_widget(btn)
        # root = ScrollView(size_hint=(None, None), size=(600, 400), pos_hint={'center_x': .5, 'center_y': .5})
        # root.add_widget(layout)
        #return root

    def on_pause(self):
        return True

    def close_app(self):
        pass

if __name__ == '__main__':
    ScopServApp().run()
