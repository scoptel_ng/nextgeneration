import pika
import json

class RabbitMQ():
    '''
    '''
    def __init__(self):
        self.connection = None
        self.amiChannel = None
        self.extensionChannel = None
        self.agentChannel = None
        self.queueChannel = None

    def connect(self):
        try:
            self.connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
            self.amiChannel = self.createChannel(exchange='AMI', type='fanout', durable=False)
            self.extensionChannel = self.createChannel(exchange='ExtensionState', type='fanout', durable=False)
            self.agentChannel = self.createChannel(exchange='AgentState', type='fanout', durable=False)
            self.queueChannel = self.createChannel(exchange='QueueState', type='fanout', durable=False)
        except Exception as err:
            print('Unexpected error:', err)


    def createChannel(self, exchange, type, durable):
        try:
            if self.connection != None:
                channel = self.connection.channel()
                channel.exchange_declare(exchange=exchange, exchange_type=type, durable=durable)
                return channel
            else:
                raise Exception('RMQ Connection not established')
        except Exception as err:
            print('Unexpected Error:', err)


    def sendMessageToChannel(self, exchange, message):
        try:
            if exchange == 'AMI':
                self.amiChannel.basic_publish(
                    exchange=exchange,
                    routing_key='',
                    body=json.dumps(message))
                print('Message sent[x]:', json.dumps(message))
            elif exchange == 'ExtensionState':
                self.extensionChannel.basic_publish(
                    exchange=exchange,
                    routing_key='',
                    body=json.dumps(message))
            elif exchange == 'AgentState':
                self.agentChannel.basic_publish(
                    exchange=exchange,
                    routing_key='',
                    body=json.dumps(message))
                print('Message sent[x]:', json.dumps(message))
            elif exchange == 'QueueChannel':
                self.queueChannel.basic_publish(
                    exchange=exchange,
                    routing_key='',
                    body=json.dumps(message))
                print('Message sent[x]:', json.dumps(message))
            else:
                raise Exception('Exchange not specified')
        except Exception as err:
            print('Unexpected Error:', err)