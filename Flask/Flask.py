from flask import Flask, jsonify, make_response, request, abort
from panoramisk import Manager
import asyncio
import pprint
app = Flask(__name__)
manager = Manager(
    loop=asyncio.get_event_loop(),
    host='172.17.0.250',
    port='5038',
    username='admin',
    secret='amp111'
    )
@app.route('/api/ping', methods=['GET'])
def ping():
    return make_response(jsonify({'success': True, 'ping': 'pong'}))
@app.route('/api/ami/ping', methods=['GET'])
def pingAMI():
    resp = manager.send_action({"Action": "Ping"})
    return make_response(jsonify({'resp': resp}))
@app.errorhandler(404)
def notFound(error):
    app.logger.warning('Error 404 :', error)
    return make_response(jsonify({'error': 'Not found'}), 404)
@app.errorhandler(501)
def internalError(error):
    app.logger.error('Internal Server Error 501:', error)
    return make_response(jsonify({'error': 'Internal Server Error'}), 501)
if __name__ == '__main__':
    manager.connect()
    app.run(debug=True)