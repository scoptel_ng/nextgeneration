import PyQt5.uic
import os

##--
__title__ = 'ScopServNG_QtDesigner2Python'
__author__ = '@ScopServ International Inc. --> Maxime Ferron / 2017-2018'
BuiltVersion = "0.01"
PyCharmVersion = "2017.2.4"
DebugMode = True
##--


PyQt5.uic.compileUiDir(os.path.dirname(__file__))